/* xperiment */

module.exports = function(grunt) {

    grunt.loadNpmTasks('grunt-collection');

    grunt.initConfig({

        uglify: {

            options: {
                mangle: false,
                compress: {
                    drop_console: true
                }
            },

            three: {
                files: {
                    'js/scripts.min.js': [

                        //"js/Tween.js",

                        //"js/binauralbeat.min.js",

                        'js/raf.js',
                        'js/rStats.js',
                        'js/dat.gui.js',
                        "js/detector.js",
                        "js/three.min.js"

                    ]
                }
            },

            all: {
                files: {
                    'js/red-moon.min.js': [

                        //"js/Tween.js",
                        'js/raf.js',
                        'js/rStats.js',
                        'js/dat.gui.js',
                        "js/detector.js",
                        "js/three.min.js",
                        //R"js/binauralbeat.min.js",
                        "js/red-moon.js"
                    ]
                }
            }
        },

        watch: {
            style: {
                files: ['../src/css/style.css'],
                tasks: ['cssmin:style'],
                options: {
                    spawn: false,
                },
            },
        }

    });

    grunt.registerTask('default', ['uglify:all']);

};
