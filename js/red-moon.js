var gui, scene, camera, render, container, orbit, mesh, material, renderer, stats, clock;

var windowHalfX = window.innerWidth / 2;
var windowHalfY = window.innerHeight / 2;

var lights = [];

var max_width = 1024;

var time, delta;

var ui = {
    volume: .85,
    binaural: 5,
    balance: .5,
    frequency:200
}

var audio, bbeat, volume;

var is_audio;

   var r_node , l_node, vol_node, cmp_node, lr_node, pan_node, listener 
    var base_fq= 100;


/* */

function init() {

    if (!Detector.webgl) {
        Detector.addGetWebGLMessage();
    }

    var h = window.innerHeight,
        w = window.innerWidth;//< max_width ? window.innerWidth : max_width;

    gui = new dat.GUI();

    scene = new THREE.Scene();

    camera = new THREE.PerspectiveCamera(45, w / h, 0.1, 250);
    camera.aspect = w / h;

    camera.lookAt(scene.position);


    clock = new THREE.Clock();
    renderer = new THREE.WebGLRenderer({
        antialias: false,
        preserveDrawingBuffer: true
    });

    //var devicePixelRatio = window.devicePixelRatio || 1; // Evaluates to 2 if Retina
    //console.log(devicePixelRatio)
    //renderer.enableScissorTest ( true );
    //renderer.setScissor(0, 60, w, h);

    renderer.gammaInput = true;
    renderer.gammaOutput = true;
    renderer.setSize(w, h);
    renderer.setViewport(0, 0, w, h);
    renderer.setClearColor(0x000000);

    document.getElementById("canvas").appendChild(renderer.domElement);

    /*controls *

    controls = new THREE.TrackballControls(camera, renderer.domElement);
    controls.rotateSpeed = 0.5;
    controls.zoomSpeed = .8;
    controls.panSpeed = 0.8;

    controls.noZoom = false;
    controls.noPan = false;
    //controls.staticMoving = true;
    controls.dynamicDampingspeed = 0.3;
    //controls.addEventListener( 'change', render )
    */

    /* lights */

    lights[0] = new THREE.PointLight(0xffffff, 1, 100)
    lights[0].position.set(1, 1, 1);

    lights[1] = new THREE.PointLight(0xff0000, .15, 100)
    lights[1].position.set(1, 0, 1);

    lights[2] = new THREE.PointLight(0x00ffff, .15, 100)
    lights[2].position.set(15, 195, 200);

    lights[3] = new THREE.AmbientLight(0x222222); // 0.2

    for (var i = 0; i <= lights.length - 1; i++) {
        scene.add(lights[i]);
    }

    /*stats */

    stats = new rStats({
        values: {
            frame: {
                caption: 'Total frame time (ms)',
                over: 16
            },
            raf: {
                caption: 'Time since last rAF (ms)'
            },
            fps: {
                caption: 'Framerate (FPS)',
                below: 40
            }
        }
    });

    /* ui */

    //var folder = gui.addFolder('controls');
    //folder.add(ui, 'speed', .25, .95).step(.001).onChange(update_ui);
    //folder.open();

   

    /* mesh */

    material = new THREE.ShaderMaterial({
        uniforms: {
            time: {
                type: "f",
                value: 0.0
            },
            size: {
                type: "v2",
                value: new THREE.Vector2(w, h)
            }
        },
        vertexShader: document.getElementById('shader-vs').textContent,
        fragmentShader: document.getElementById('shader-fs').textContent,
        lights: false
    });

    mesh = new THREE.Mesh(new THREE.PlaneBufferGeometry(50, 32, 32), material);
    camera.position.set(0, 0, 25);
    scene.add(camera);
    scene.add(mesh);

    /* audio/binaural part */

 
   


      try {
        // Fix up for prefixing       
        is_audio = !!window.AudioContext;
        //window.AudioContext = window.AudioContext || window.webkitAudioContext;     
        
    } catch (e) {
        alert('Web Audio API is not supported in this browser. Grab Chrome!');
    }

    if ( is_audio ) {

            var ctx = new AudioContext();

           
            r_node = ctx.createOscillator();    
            r_node.waveType = 'sine';
            r_node.frequency.value = ui.frequency;            

            l_node = ctx.createOscillator();
            l_node.waveType = 'sine';
            l_node.frequency.value = ui.frequency + ui.binaural;

            //panner node

            pan_node = ctx.createPanner();
            pan_node.panningModel = 'HRTF';
            pan_node.distanceModel = 'inverse';
            pan_node.refDistance = 1;
            pan_node.maxDistance = 1000;
            pan_node.rolloffFactor = 1;
            pan_node.coneInnerAngle = 360;
            pan_node.coneOuterAngle = 0;
            pan_node.coneOuterGain = 0;
            pan_node.setPosition(w/2, h/2, 0);
            pan_node.setOrientation(1,0,0);
    

            listener = ctx.listener;
            listener.setOrientation(0, 0, -1, 0, 1, 0);
            //console.log(w,h)
            listener.setPosition(w/2, h/2, 0);

            //gain node
            vol_node= ctx.createGain();
            vol_node.gain.value = ui.volume;

            //compressor node
            cmp_node = ctx.createDynamicsCompressor();

            //merger node canali
            //lr_node = ctx.createChannelMerger(2);

            //--> comp
            l_node.connect( cmp_node );
            r_node.connect( cmp_node );

            //--> vol 
            cmp_node.connect( vol_node );

            //--> merge
            //vol_node.connect(lr_node);

            //--> pan
            vol_node.connect(pan_node);

            // --> out
            pan_node.connect(ctx.destination);

            //start osc

            r_node.start(0);
            l_node.start(0);

        }

        var folder_audio = gui.addFolder('binaural');

            //https://de.wikipedia.org/wiki/Planetent%C3%B6ne

            folder_audio.add(ui, 'frequency', base_fq, 250).step(.01).onChange(update_ui);
            folder_audio.add(ui, 'volume', 0.5, 1).step(.01).onChange(update_ui);
            //folder_audio.add(ui, 'balance', 0, 1).step(.01).onChange(update_ui);
            folder_audio.add(ui, 'binaural', 0, 20).step(.1).onChange(update_ui);
            folder_audio.open()


    render();
    window.addEventListener('resize', onWindowResize, false);
}

function lerp(ratio , start , end) { return start + (end - start) * ratio; }

function norm(val , start , end) { return ( (val-start) / (end-start) ) ; }

/*@param balance - 0..1 */
function update_panner( balance){
    var w = window.innerWidth;
    var h = window.innerHeight;

    var dx = 0//(balance <.5 ) ? -lerp(balance, 0, w/2) : lerp(balance, w/2, w);

    //console.log(dx)

    //pan_node.setPosition(dx, h/2, 0);

}

function update_ui() {
    // material.uniforms.speed.value = data.speed;
    vol_node.gain.value = ui.volume;

    l_node.frequency.value = ui.frequency;
    r_node.frequency.value = ui.frequency +  ui.binaural;

    update_panner( ui.balance);
    

}

/* render */
var t=0;
function render() {

    stats('frame').start();
    stats('rAF').tick();
    stats('FPS').frame();

    delta = clock.getDelta();

    requestAnimationFrame(render);
    
    t+= delta *  norm(ui.frequency, 100, 250);

    material.uniforms.time.value = t 

    if ( is_audio ) {
       /* var pitch = 420 + 10*Math.sin(t)*Math.cos(t);*/
   
    }
    
    renderer.render(scene, camera);

    stats('frame').end();
    stats().update();

};


window.addEventListener('load', function() {
    init();
})

function onWindowResize() {

    var h = window.innerHeight,
        w = window.innerWidth;// < max_width ? window.innerWidth : max_width;

    material.uniforms.size.value = new THREE.Vector2(w, h);

    camera.aspect = w / h;
    camera.updateProjectionMatrix();
    renderer.setSize(w, h);
    renderer.setViewport(0, 0, w, h);
    renderer.render(scene, camera);
}
