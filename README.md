## Red Moon
red sin/cos binaural meditation.
tech stack: ThreeJS + custom shader,  Web Audio API.

#### Your browser has WebGL support? check it
[webglreport](http://webglreport.com/?v=1]) or [can i use](http://caniuse.com/#feat=webgl)

## demo :
[http://spleennooname.github.io/redmoon-threejs-webaudio/index.html](http://spleennooname.github.io/redmoon-threejs-webaudio/index.html)
